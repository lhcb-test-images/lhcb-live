LHCb Live CD setup
==================


Based on the CC7  one kindly provided by Jarek and Thomas from CERN/IT

https://gitlab.cern.ch/linuxsupport/cc7-live

Description
-----------------

This repository produces the bootable ISO image:

LHCb-LiveCD.iso

Booting on this image, you start a centos7 system, which contains a version
of the LHCb upgrade test setup (including 1GB data file).

After the boot sequence you end up in a shell, and you can start the machine
test by running:

```
/opt/test/runtest.x86_64+avx2+fma-centos7-gcc62-opt
```

for the optoimized code with AVX2 and FMA, and

```
/opt/test/runtest.x86_64-centos7-gcc62-opt
```
For the test requiring only x86_64 and SSE4.2

Build Requirements
------------------

  - Needs to be run on a Centos7 host, as root.
  - Requires the livecd-tools to be installed 

Build Description
-----------------

The keystart file lhcb-livecd.ks installs the necessary RPMs on the installation (python-devel,
python-pip, HEP_OSlibs, numactl) then copies to /opt the content of the LHCb installation as created by:

https://gitlab.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon

and exported by:

https://gitlab.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon-export

The file *LHCb-LiveCD.iso* is produced.


How to build
------------

Use:

https://gitlab.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon-export

to export the LHCb installation tar files and copy them to:

  - /var/tmp/LHCbSoft.tar.gz
  - /var/tmp/LHCbCond.tar.gz
  - /var/tmp/LHCbTest.tar.gz
  - /var/tmp/LHCbNightlies.tar.gz

Check the last %post at the end of the .ks file, to change the file locations.

Then invoke:

```
make
```

BEWARE: the build process drops you in a shell to inspect the installation at the end.
Just call CTRL-D to exit.




