all: iso

iso:
	LANG=C livecd-creator --config=lhcb-livecd.ks --fslabel="LHCb-LiveCD" --tmpdir=/var/tmp/lhcb-live-tmp/ --cache=/var/tmp/lhcb-live-cache/ --debug

publish:
	scp LHCb-LiveCD.iso bcouturi@lxplus7:/eos/project/l/lhcbwebsites/www/lhcb-rpm/iso

iso2disk:
	livecd-iso-to-disk --format --reset-mbr --unencrypted-home --home-size-mb 16000 --noverify LHCb-LiveCD.iso /dev/sdb
